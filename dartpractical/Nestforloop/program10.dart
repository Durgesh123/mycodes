import "dart:io";
/*void main(){

	int row = 5;
        int num = 1;
	   for(int i = 0; i < row ; i++){
               
		for(int j = 0;j <= i ; j++){

                	  stdout.write(i);  
                          num++;
		}
 
			//num+=1;
 	               	  print("");

	}	
*/
void main() {
  int n = 6; // Total number of rows

  for (int i = n; i >= 1; i--) {
    int num = i;
    int increment = i;

    for (int j = 1; j <= (n - i + 1); j++) {
      stdout.write('$num ');
      if (j % 2 != 0) {
        num = increment; // Keep the current incremented value for even positions
      } else {
        num = j + (n - i + 1); // For odd positions, update with new increment
      }
    }
     print(" ");
  }
 //  print(" ");
}

