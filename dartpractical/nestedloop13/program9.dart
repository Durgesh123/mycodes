import "dart:io";

int? fun(int num){
	int sum = 0;
	int temp = num;
	while(temp > 0){
	   sum+= temp % 10;
	   temp = temp ~/ 10;
        }
	if(sum > 0 && num % sum == 0)
		return num;
				
}

void main(){

	int row = 4; int num = 1;

	for(int i = 0; i < row; i++){
		for(int j = 0; j < row; j++){

		        int? res = fun(num);
			if(res != null)
			    stdout.write("${res} ");
			
                        num++;	
		}
			print("");

	}


}
