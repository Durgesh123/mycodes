import "dart:io";
String fun(int number) {
  if (number == 0) return "0"; 
  String binary = ""; 

  while (number > 0) {
    int remainder = number % 2;  
    binary = remainder.toString() + binary; 
    number = number ~/ 2; 
  }

  return binary;
}

void main() {
  
     int row = 4;
      int cnt = 1;
	for(int i = 0; i < row; i++){
		for(int j = 0; j < row; j++){
		
			String num = fun(cnt);
			stdout.write("${num}  ");
			cnt++;
		
			}

		print(" ");
	}
  
  
}

