void main(){

	int num = 145;
	int org = num;
	int sum = 0;

	while(num > 0){
	    int digit = num % 10;
	    int mul = 1;
	        for(int i = 1; i <= digit; i++){

	                   mul*=i;

	        }
	
             sum+=mul;
	     num = num ~/ 10;

	}

	if( org == sum)
		print("${org} is strong number");
	else
		print("${org} is not strong number");
}
