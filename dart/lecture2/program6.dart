// arithmetic op (+,-,*,%,/,~,~/)

void main(){
 
   int x = 10;
   int y = 5;

   print(x + y);// 15
   print(x - y);//5
   print(x * y);//50
   print(x / y);//2.0
   print(x % y);//0
   print(x ~/ y);//2

}
