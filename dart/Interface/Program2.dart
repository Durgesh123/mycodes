abstract class parent1{
	void fun(){
		print("in fun-parent");
	}
}
abstract class parent2{
	void run(){
		print("in fun-parent");
	}
}
class child implements parent1,parent2{

 // <-- error:the non abstract class child is missing implementation for these memebers : parent1.fun,p -run

}
void main(){

	child ch = child();
	ch.fun();
        ch.run();

}
