class parent1{
        
	parent1(){

		print("parent1 constructor");
	}
}
class parent2{
	parent2(){

		print("parent2 constructor");
	}
}
class demo extends parent1{
	demo(){
		print("demo constructor");
	}

}
class child implements parent1{

	child(){
		print("child constructor");
	}  					// code succesfully  run
}
void main(){

	child ch = child();
}
