class parent1{

	int x = 10;
	void fun(){
		print("parent1 constructor");
		print(x);
	}
}
class demo implements parent1{

                      //<--- x missing implementation for these members - parent1.x	
	void fun(){
		print("demo constructor");
	}
}
void main(){

	demo dm = new demo();
	dm.fun();

}
