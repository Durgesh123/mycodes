class program1{

     int? x;
     String? name;

    void fun(int x,String name){

       this.x = x;
       this.name = name;

    

   print(" $x  $name ");
  }
}
void main(){


   // in dart you can create 4 type of object :

   program1  pg = new program1();//1
   pg.fun(18,"virat");

   program1  pg1 = program1();//2
   pg1.fun(18,"virat");

    new program1().fun(18,"virat");//3
    
    program1().fun(18,"virat"); //4

}
