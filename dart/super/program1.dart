class parent{

  int x = 10;
 
  parent(){
  
   print("parent constructor");
   print(x); 

  }

  int get getX => x;
  
  void set setX(int data){
      x = data;
  } 

}
class child extends parent{

  int x = 20;

  child(){
           print("child constr");
           print(x);
    
  }

}
void main(){

    child obj = new child();
    obj.setX = 33;
    print(obj.getX);
}
