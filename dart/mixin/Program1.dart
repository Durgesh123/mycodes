abstract class demo{

	void fun();
}
abstract class Memo{

	void fun();
}
mixin funmethod on demo{
	
	void fun(){

		print("in fun - mixin");
	}

}
class child extends demo with funmethod{



}
void main(){

	child obj = child();
	obj.fun();
}
