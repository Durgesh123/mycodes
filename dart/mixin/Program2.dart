mixin demo{
	static int a = 50;	
	int x = 10;
	void fun(){

		print("in fun demo");
		print(x);
	}

}
mixin memo{

	int y = 20;
	void run(){

		print("in run demo");
		print(y);
	}

}
class child with memo,demo{

	
}
void main(){

	child ch = child();
	ch.run();
	ch.fun();
	print(demo.a);

}
