void fun(int x,[int y = 20],{double d = 10.5}){// error in this code because name and optional parameter can be used at a time.

    print(x);
    print(y);
    print(d);
}
void main(){
  
     fun(10);
}
