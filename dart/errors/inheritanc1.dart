/*class test{

  int? x;
  static int y = 20;
  Test.initX(this.x);

  static void changeY(){

	y = 30;
 } 
}
class Test2 extends Test{

    Test2(int x):super.init(X);
}
void main(){

  Test obj = Test2(40);
  Test2.changeY();  <--- error in this code, correct code given below
  print(Test2.y);

}*/
class Test {
  int? x;
  static int y = 20;

  Test.initX(this.x);

  static void changeY() {
    y = 30;
  }
}

class Test2 extends Test {
  Test2(int x) : super.initX(x);
}

void main() {
  Test obj = Test2(40);
  Test.changeY();
  print(Test.y);
}

