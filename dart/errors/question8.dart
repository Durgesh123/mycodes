abstract class parent {

	static int z = 9;
	void fun(){

		print("in fun");
		gun();
	}

	static void sun();  // <= compiletime error
	void gun();
}
class child extends parent{

	void fun(){

		super.fun();
	}

	void gun(){

		print("in gun");
	}

	static void sun(){}
}
 void main(){

	child obj = new child();
}
