abstract class demo{

	void fun();
} 
abstract class memo extends demo{

	void fun();
}
mixin funmethod on demo{

	void fun(){

		print("in fun - mixin");
	}

}
class child extends memo with funmethod{}

void main(){

	child obj = child();
	obj.fun();

}
