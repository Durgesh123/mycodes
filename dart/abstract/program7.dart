abstract class parent{

     void run(){
        print("parent - fun()");
  }
}
class child extends parent{

      void fun(){
          print("child - fun()");
      }
}      
void main(){
	
	child ch = child();
        ch.fun();
        ch.run();  

}
