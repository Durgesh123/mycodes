abstract class parent{

     void fun();
}
abstract class child extends parent{

	void fun(){
	print("in fun()");
     }

}
class child2 extends child{

      void fun(){
          print("child - in fun()");
      }
}      
void main(){
	
	child2 ch = child2();
        ch.fun();  

}
