abstract class parent{

     void fun();
}
class child extends parent {}  // <-- the non abstract class 'demo' is missing implementation for these members.

void main(){

   child ch = child();
}
