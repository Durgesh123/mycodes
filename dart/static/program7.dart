class demo{

   static int x = 10;
    int get getX => x;

   void set setX(int data) => x = data;
}

void main(){

  demo obj = demo();
  demo obj1 = demo();
  print(obj.getX);//10
  print(obj1.getX);//10

  obj1.setX = 0;//5
  print(obj.getX); // ans 5 bec static varible is comman

}
