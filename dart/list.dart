void main(){

	List num = [1,2,3,4,5,6];
	print(num);


print("--------------List.property----------------------");


	print("first element : ${num.first}");
        print("first last    : ${num.last}");
	print("length : ${num.length}");
	print("isEmpty : ${num.isEmpty}");
	print("isNotEmpty : ${num.isNotEmpty}");
	print("revers : ${num.reversed}");


print("--------------List.method---------------------");


	print(num);
	
	num.add(9);
	print("add element :${num}");

	num.insert(2,15);
	print("insert element :${num}");

	num.addAll([21,22]);
	print("add all element :${num}");

	num.insertAll(3,[16,17,18,19,20]);
	print("addAll element :${num}");

	print("index 19 :${num.indexOf(19)}");
	print("lastindexOf 22 :${num.lastIndexOf(22)}");
	
	num.removeRange(2,7);
	print("removeRange 2-7 :${num}");

	print(num.runtimeType);


}
	
